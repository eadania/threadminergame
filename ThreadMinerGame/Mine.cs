﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadMinerGame
{
    public class Mine : INotifyPropertyChanged
    {
        /// <summary>
        /// Eventhandler for whenever a property is changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// A semaphore for the mine 
        /// </summary>
        public Semaphore mineSemaphore; 
        /// <summary>
        /// Bool if new mine is found = true or false
        /// </summary>
        public bool Found;
       
        private int coalAmount;
        private int workerCap;
        private int currentWorkers;
        private int workTime;
        private int exhaust;
        private string name;
        private int distanceToMine;
        private MineManager mineManagerRef;

        /// <summary>
        /// Amount of coal
        /// </summary>
        public int CoalAmount
        {
            get => coalAmount;
            set
            {
                coalAmount = value;
                if (coalAmount < 0)
                {
                    coalAmount = 0;
                }
                OnPropertyChanged("CoalAmount");
            }
        }
        /// <summary>
        /// The maximum amount of workers that can be in the mine at a time.
        /// </summary>
        public int WorkerCap { get => workerCap; set { workerCap = value; OnPropertyChanged("WorkerCap"); } }
        /// <summary>
        /// The amount of current workers in the mine
        /// </summary>
        public int CurrentWorkers { get => currentWorkers; set { currentWorkers = value; OnPropertyChanged("CurrentWorkers"); } }
        /// <summary>
        /// The amount of time it takes for them to obtain coal.
        /// </summary>
        public int WorkTime { get => workTime; set { workTime = value; OnPropertyChanged("WorkTime"); } }
        /// <summary>
        /// The amount of energy the worker loses
        /// </summary>
        public int Exhaust { get => exhaust; set { exhaust = value; OnPropertyChanged("Exhaust"); } }
        /// <summary>
        /// The amount of distance to the mine
        /// </summary>
        public int DistanceToMine { get => distanceToMine; set { distanceToMine = value; OnPropertyChanged("DistanceToMine"); } }
        /// <summary>
        /// The Name on the worker
        /// </summary>
        public string Name { get => name; set { name = value; OnPropertyChanged("Name"); } }



        // public int CarryAmount;
        /// <summary>
        /// Method that makes the Workers enters the mine if condition is met
        /// </summary>
        /// <param name="Bob">A worker that enters the mine</param>
        /// <param name="CoalToHq">Specifies the amount of coal the hq recieves</param>
        public void EnterMine(Worker Bob, HeadquartersManager CoalToHq)
        {
            while (Bob.Energy >= 1)
            {
                if (mineSemaphore.WaitOne(1))
                {
                    try
                    {
                        currentWorkers += 1;

                        int oldBalance = CoalAmount;

                        CoalAmount -= 200;
                        Bob.Energy -= Exhaust;
                        CoalToHq.CoalBalance += oldBalance - CoalAmount;
                        Thread.Sleep(WorkTime);

                        currentWorkers -= 1;
                    }
                    finally
                    {
                        mineSemaphore.Release();
                    }
                }

                if (coalAmount <= 0)
                {
                    mineManagerRef.SearchForNewMine(Bob);
                    break;
                }
            }
        }


        /// <summary>
        /// A constructor for the mine 
        /// </summary>
        /// <param name="CoalAmount">How much Coal</param>
        /// <param name="WorkerCap">The maximum numbero of workers in a mine at a time</param>
        /// <param name="WorkTime">The amount of time it takes for a worker to obtain coal</param>
        /// <param name="Exhaust">How much energy the worker loses</param>
        /// <param name="DistanceToMine">The distance from a workers home to their mine</param>
        /// <param name="mineInc">The length on the list of mines</param>
        /// <param name="mineMgrRef">Minemanager reference</param>
        public Mine(int CoalAmount, int WorkerCap,int WorkTime, int Exhaust, int DistanceToMine, int mineInc, MineManager mineMgrRef)
        {
            this.CoalAmount = CoalAmount;
            this.WorkerCap = WorkerCap;
            this.currentWorkers = 0;
            this.WorkTime = WorkTime;
            this.Exhaust = Exhaust;
            this.distanceToMine = DistanceToMine;
            this.name = "Mine #" + mineInc;

            this.mineSemaphore = new Semaphore(workerCap, workerCap);
           

            this.mineManagerRef = mineMgrRef;
        }

        protected void OnPropertyChanged(string AName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if(handler != null)
            {
                handler(this, new PropertyChangedEventArgs(AName));
            }
        }
    }
}
