﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ThreadMinerGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Tavern bar = new Tavern(0, 5, 0,10,100);

        HeadquartersManager myHeadquater;

        /// <summary>
        /// Reference to the mineManager instance
        /// </summary>
        public MineManager mineManager;

        /// <summary>
        /// List of workers
        /// </summary>
        public ObservableCollection<Worker> MyWorkers = new ObservableCollection<Worker>();

        /// <summary>
        /// Creates a new instance of the MainWindow class
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = bar;



            myHeadquater = new HeadquartersManager(0, 180, 1000, 20000, this);

            SetDataContextForHeadquater();

            mineManager = new MineManager(5000, myHeadquater);

            SetDataContextForMines();

            MyWorkers.Add(new Worker(100, MyWorkers.Count, mineManager.mines.First(), bar, myHeadquater));
            SetDataContextForWorkers();

            myHeadquater.GameOver += OnGameOver;

        }

        /// <summary>
        /// Handles onClick event for Headquaters menu button
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The event</param>
        private void Headquaters_Click(object sender, RoutedEventArgs e)
        {
            CoalMinesGrid.Visibility = Visibility.Collapsed;
            BarGrid.Visibility = Visibility.Collapsed;
            HeadquaterGrid.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Handles onClick event for CoalMines menu button
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The event</param>
        private void Coal_Mine_Click(object sender, RoutedEventArgs e)
        {
            CoalMinesGrid.Visibility = Visibility.Visible;
            BarGrid.Visibility = Visibility.Collapsed;
            HeadquaterGrid.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Handles onClick event for Bar menu button
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The event</param>
        private void Bar_Click(object sender, RoutedEventArgs e)
        {
            CoalMinesGrid.Visibility = Visibility.Collapsed;
            BarGrid.Visibility = Visibility.Visible;
            HeadquaterGrid.Visibility = Visibility.Collapsed;
        }

        private void BuyBeerButton_Click(object sender, RoutedEventArgs e)
        {
            if (myHeadquater.CoalBalance < bar.Price)
            {

            }
            else
            {
                bar.Beer += 1;
                myHeadquater.CoalBalance -= bar.Price;
            }
        }

        /// <summary>
        /// Sets the data context for headquaters window
        /// </summary>
        private void SetDataContextForHeadquater()
        {
            Amountoftotalcoal.DataContext = myHeadquater;
            DaysLeft.DataContext = myHeadquater;

            HeadquaterInfo.DataContext = myHeadquater;
            Priceofworker2.DataContext = myHeadquater;
        }

        private void SetDataContextForMines()
        {
            MinesListView.ItemsSource = mineManager.mines;
        }

        private void SetDataContextForWorkers()
        {
            WorkerList.ItemsSource = MyWorkers;
        }

        public void AddNewWorker()
        {
            MyWorkers.Add(new Worker(100, MyWorkers.Count, mineManager.mines.FirstOrDefault(), bar, myHeadquater));
        }

        /// <summary>
        /// Handles onClick event for buying a new worker
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The event</param>
        private void BuyWorkBut_Click(object sender, RoutedEventArgs e)
        {
            myHeadquater.HireWorker();
            
        }

        private void OnGameOver(string Message)
        {
            // Dispatch method to hide window to main thread
            this.Dispatcher.Invoke(() =>
            {
                this.Hide();
            });

            if (Message == "Bad")
            {
                MessageBox.Show("HAHAHAHAHAHAH NU HAR DU TABT!!!! >_> <_<");
            }
            else if(Message == "Good")
            {
                MessageBox.Show("Hahahaha du vandt, godt gaeet eller noget");
            }
            // Inform player they lost
          
            // Dispatch method to close application to main thread
            this.Dispatcher.Invoke(() =>
            {
                this.Close();
            });
        }

        /// <summary>
        /// Method to remove a mine from the list
        /// </summary>
        /// <param name="aMineToRemove"></param>
        public void RemoveAMine(Mine aMineToRemove)
        {
            this.Dispatcher.Invoke(() =>
            {
                mineManager.mines.Remove(aMineToRemove);

                foreach (Worker worker in MyWorkers)
                {
                    worker.ChangeMine(mineManager.mines.FirstOrDefault());
                }
            });            
        }
        /// <summary>
        /// Method to check or a new mine > No longer in use
        /// </summary>
        /// <param name="worker">The worker who needs a mine</param>
        public void CheckForNewMine(Worker worker)
        {
            mineManager.SearchForNewMine(worker);

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            mineManager.Prospect();
        }
    }
}
