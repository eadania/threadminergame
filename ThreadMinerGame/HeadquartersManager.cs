﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadMinerGame
{
    public delegate void GameOverHandler(string end);

    public class HeadquartersManager : INotifyPropertyChanged
    {
        private int coalBalance;
        private int daysLeft;
        private int workerPrice;
        private int dayLength;

        private int coalGoal;

        
        private Thread passTimeThread;

        /// <summary>
        /// Reference to the Main Windows class
        /// </summary>
        public MainWindow MainWindowRef;
        /// <summary>
        /// Event handler for whenever a property is changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Event handler for whenever the game is finished
        /// </summary>
        public event GameOverHandler GameOver;

        /// <summary>
        /// The property to display coal balance
        /// </summary>
        public int CoalBalance
        {
            get { return coalBalance; }
            set
            {
                coalBalance = value;
                OnPropertyChanged("CoalBalance");
            }
        }

        /// <summary>
        /// Property to display days left
        /// </summary>
        public int DaysLeft
        {
            get { return daysLeft; }
            set {
                daysLeft = value;

                if (daysLeft == 0)
                {
                    GameOver("Bad");
                    //ResetGame();
                }

                OnPropertyChanged("DaysLeft");
            }
        }

        /// <summary>
        /// The price for hiring a new worker
        /// </summary>
        public int WorkerPrice { get => workerPrice; set => workerPrice = value; }

        /// <summary>
        /// The amount of coal to reach in order to win the game
        /// </summary>
        public int CoalGoal
        {
            get { return coalGoal; }
            set
            {
                coalGoal = value;
                OnPropertyChanged("CoalGoal");
            }
        }

        /// <summary>
        /// Constructor for the HeadquatersManager
        /// </summary>
        /// <param name="cBalance">Starting coal balance</param>
        /// <param name="startingDays">Amount of days left when starting</param>
        /// <param name="dayLength">Time between days in milliseconds</param>
        public HeadquartersManager(int cBalance, int startingDays, int dayLength, int coalGoal, MainWindow mainRef)
        {
            this.coalBalance = cBalance;
            this.daysLeft = startingDays;

            this.workerPrice = 3000;
            this.dayLength = dayLength;

            this.coalGoal = coalGoal;

            this.MainWindowRef = mainRef;

            passTimeThread = new Thread(this.PassTime);
            passTimeThread.IsBackground = true; // Make sure the thread runs in background
            passTimeThread.Start(dayLength);
        }

        /// <summary>
        /// Deducts coal balance if affordable and prepares to hire a new worker
        /// </summary>
        public void HireWorker()
        {
            if (coalBalance >= workerPrice)
            {
                CoalBalance = CoalBalance - workerPrice;

                this.MainWindowRef.AddNewWorker();
                // Create new worker here..
            }
        }
        
        /// <summary>
        /// Private method to tick days away over time
        /// </summary>
        /// <param name="o">number of milliseconds between each day</param>
        private void PassTime(object o)
        {
            int sleep = (int)o;

            while (true)
            {
                if (coalBalance >= coalGoal)
                {
                    GameOver("Good");
                }
                Thread.Sleep(sleep);

                DaysLeft--;
            }
        }

        /// <summary>
        /// Used to reset the game > No longer implemented
        /// </summary>
        public void ResetGame()
        {
            daysLeft = 45;
        }

        /// <summary>
        /// Delegate to notifiy GUI whenever a property has changed
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
