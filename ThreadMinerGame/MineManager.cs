﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreadMinerGame
{
    public class MineManager
    {
        private int prospectCost;
        /// <summary>
        /// A ObservableCollection for the mines.
        /// </summary>
        public ObservableCollection<Mine> mines = new ObservableCollection<Mine>();

        private Queue<Mine> hiddenMines = new Queue<Mine>();
        private HeadquartersManager hqRef;
        /// <summary>
        /// The cost for searching for a new mine
        /// </summary>
        public int ProspectCost { get => prospectCost; set => prospectCost = value; }
        /// <summary>
        /// A constructer for the MineManager class
        /// </summary>
        /// <param name="prospectCost">The cost for searching for a new mine</param>
        /// <param name="headquarter">Headquarter reference</param>
        public MineManager(int prospectCost, HeadquartersManager headquarter)
        {
            this.ProspectCost = prospectCost;
            this.hqRef = headquarter;


            mines.Add(new Mine(7053, 2, 1000, 9, 10, mines.Count, this));
            hiddenMines.Enqueue(new Mine(9535, 2, 500, 7, 14, mines.Count + hiddenMines.Count, this));
            hiddenMines.Enqueue(new Mine(18459, 2, 700, 5, 7, mines.Count + hiddenMines.Count, this));
        }
               
        /// <summary>
        /// This removes the empty mine
        /// </summary>
        /// <param name="aMineToRemove">Mine reference</param>
        public void RemoveEmptyMine(Mine aMineToRemove)
        {
            hqRef.MainWindowRef.RemoveAMine(aMineToRemove);
        }
        /// <summary>
        /// The method for searching for a new mine
        /// </summary>
        /// <param name="bob">Worker reference</param>
        public void SearchForNewMine(Worker bob)
        {
            Mine aValidMine = null;
            foreach (Mine mine in mines)
            {
                if(mine.CoalAmount > 0)
                {
                    aValidMine = mine;
                    break;
                }
            }
            bob.ChangeMine(aValidMine);
        }
        /// <summary>
        /// The method for searching for a new mine.
        /// </summary>
        public void Prospect()
        {
            if (hqRef.CoalBalance >= prospectCost && hiddenMines.Count > 0)
            {
                hqRef.CoalBalance -= prospectCost;

                mines.Add(hiddenMines.Dequeue());
            }
        }

    }
}
