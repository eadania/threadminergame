﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;

namespace ThreadMinerGame
{    
    public class Worker : INotifyPropertyChanged
    {
        private bool mining = true;

        private string name;
        private int energy;
        private Thread workerThread;

        private HeadquartersManager hq;

        private string currentTask;
        private Action<Worker> tavernDelegate;
        private Action<Worker, HeadquartersManager> miningDelegate;

        /// <summary>
        /// Event
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// property
        /// </summary>
        public string Name { get => name; set => name = value; }
        public int Energy
        {
            get => energy;
            set
            {
                energy = value;
                OnPropertyChanged("Energy");
            }
        }
        public string CurrentTask
        {
            get => currentTask;
            set
            {
                currentTask = value;
                OnPropertyChanged("CurrentTask");
            }
        }

        /// <summary>
        /// ChangeMine constructor
        /// </summary>
        /// <param name="mine">Delegate to a mine</param>
        public void ChangeMine(Mine mine)
        {
            if (mine != null)
            {
                this.miningDelegate = mine.EnterMine;
            }
            else
            {
                this.miningDelegate = CheckForNewMine;
            }
        }

        /// <summary>

        /// Worker constructor
        /// </summary>
        /// <param name="Energy">Workers energy</param>
        /// <param name="workerInc">The number of the worker</param>
        /// <param name="mineRef">Delegate to mine</param>
        /// <param name="tavernRef">Delegate to Tavern</param>
        /// <param name="hqRef">Delegate to HeadquartersManager</param>
        public Worker(int Energy, int workerInc, Mine mineRef, Tavern tavernRef, HeadquartersManager hqRef)
        {
            this.Energy = Energy;
            this.Name = "Worker #" + workerInc;

            workerThread = new Thread(DoSomthing);
            workerThread.IsBackground = true;
            workerThread.Start();

            this.tavernDelegate = tavernRef.DrinkBeer;
            this.miningDelegate = mineRef.EnterMine;
            this.CurrentTask = "In Mines";

            this.hq = hqRef;
        }

        void DoSomthing()
        {
            while (true)
            {
                if (this.energy >= 1 && this.miningDelegate != CheckForNewMine)
                {
                    this.CurrentTask = "In Mines";
                    this.miningDelegate(this, hq);
                }
                else if (this.energy <= 0)
                {
                    this.CurrentTask = "In Tavern";
                    this.tavernDelegate(this);
                }
                else if (this.miningDelegate == CheckForNewMine)
                {
                    this.CurrentTask = "No Mines found";
                    this.miningDelegate(this, hq);
                }
            }
        }

        private void CheckForNewMine(Worker me, HeadquartersManager hqM)
        {
            //this.CurrentTask = "No mines found";
            Thread.Sleep(100);

            hq.MainWindowRef.mineManager.SearchForNewMine(this);
        }

        /// <summary>
        /// Delegate to notifiy GUI whenever a property has changed
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
