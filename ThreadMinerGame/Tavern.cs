﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadMinerGame
{

    public class Tavern : INotifyPropertyChanged
    {
        static Object thisLock = new Object();

        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// properties as get/set so we can change the values
        /// </summary>
        private int beerValue;
        public int Beer { get { return beerValue; } set { beerValue = value; OnPropertyChanged("Beer"); } }

        private int workersMaxValue;
        public int MaxWorkers { get { return workersMaxValue; } set { workersMaxValue = value; OnPropertyChanged("MaxWorkers"); } }

        private int currentWorkers;
        public int CurrentWorkers { get { return currentWorkers; } set { currentWorkers = value; OnPropertyChanged("CurrentWorkers"); } }

        //private string combo;
        //public string Combo { get { return CurrentWorkers.ToString() + "/" + MaxWorkers.ToString(); } }

        private int timeDrink;
        public int TimeDrink { get { return timeDrink; } set { timeDrink = value; OnPropertyChanged("TimeDrink"); } }

        private int price;
        public int Price { get { return price; } set { price = value; OnPropertyChanged("Price"); } }
        /// <summary>
        /// Constructor for Tavern
        /// </summary>
        /// <param name="Beer">Amount of beer</param>
        /// <param name="MaxWorkers">Higest amount of workers inside</param>
        /// <param name="CurrentWorkers">Current amount of workers indside</param>
        /// <param name="TimeDrink">how long to drink a beer</param>
        /// <param name="Price">Price of a beer</param>
        public Tavern(int Beer, int MaxWorkers, int CurrentWorkers, int TimeDrink, int Price)
        {
            this.Beer = Beer;
            this.MaxWorkers = MaxWorkers;
            this.CurrentWorkers = CurrentWorkers;
            this.TimeDrink = TimeDrink;
            this.Price = Price;
        }
        /// <summary>
        /// Entering the tavern
        /// </summary>
        public void EnterTavern()
        {
            if (CurrentWorkers > 5)
            {
                CurrentWorkers += 1;
            }
        }
        /// <summary>
        /// When someone drinks a beer
        /// </summary>
        public void DrinkBeer(Worker bob)
        {
            Monitor.Enter(thisLock);
            try
            {
                while (Beer > 0 && bob.Energy < 100)
                {
                    Beer -= 1;
                    bob.Energy += 25;
                    Thread.Sleep(1000);
                }
            }
            finally
            {
                Monitor.Exit(thisLock);
            }
        }
        /// <summary>
        /// Changes out properties everytime a change is made
        /// </summary>
        /// <param name="Name">Navnet på den ændrede værdi</param>
        protected void  OnPropertyChanged(string Name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if(handler != null)
            {
                handler(this, new PropertyChangedEventArgs(Name));
            }
        }
    }
}
